import {TestBed} from '@angular/core/testing';
import {FirebaseDatabaseService} from './firebase-database.service';
import {AppModule} from '../app.module';

describe('FirebaseDatabaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AppModule,
    ],
  }));

  it('should be created', () => {
    const service: FirebaseDatabaseService = TestBed.get(FirebaseDatabaseService);
    expect(service).toBeTruthy();
  });

  it('should return object path', () => {
  });
});
