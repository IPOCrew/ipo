import {Injectable} from '@angular/core';
import {AngularFireDatabase, AngularFireList, AngularFireObject, snapshotChanges} from '@angular/fire/database';
import {MUser} from '../models/mUser';
import {MParkingSpot} from '../models/mParkingSpot';
import {FirebaseAuthService} from './firebase-auth.service';
import {MContact} from '../models/mContact';
import {MPlot} from '../models/mPlot';
import {Observable, Subscription} from 'rxjs';
import {MTenant} from '../models/mTenant';

@Injectable({
  providedIn: 'root'
})
export class FirebaseDatabaseService {

  public currentPlotID: string;

  constructor(public db: AngularFireDatabase, public authService: FirebaseAuthService) {
  }

  addUserToDatabase(user: MUser) {
    this.db.object('user/' + user.id).set(user).catch((errorMessage) => {
      return errorMessage;
    });
  }

  addContactToDatabase(contact: MContact) {
    this.db.object('contact/' + this.db.createPushId()).set(contact);
  }

  getCurrentUser(uid: String): Observable<any> {
    return this.db.object('user/' + uid).snapshotChanges();
  }

  addPlotToDatabase(plot: MPlot) {
    this.db.object('plot/' + plot.id).set(plot);
  }

  deletePlotFromDatabase(plot: MPlot) {
    this.db.object('plot/' + plot.id).remove();
  }

  getPlotList(): Observable<any> {
    return this.db.list('plot').snapshotChanges();
  }

  addParkingSpotToDatabase(parkingSpot: MParkingSpot) {
    this.db.object('parkingSpot/' + parkingSpot.id).set(parkingSpot);
  }

  addTenantToParkingSpot(id: string, tenant: MTenant, parkingSpot: MParkingSpot) {
    this.db.object('parkingSpot/' + parkingSpot.id + '/tenant/' + id).set(tenant);
  }

  removeTenantFromParkingSpot(tenantId: string, spotId: string) {
    this.db.object('parkingSpot/' + spotId + '/tenant/' + tenantId).remove();
  }

  deleteParkingSpotFromDatabase(parkingSpot: MParkingSpot) {
    this.db.object('parkingSpot/' + parkingSpot.id).remove();
  }

  setCurrentPlot(id: string) {
    this.currentPlotID = id;
  }

  replacePlot(id: string, reworkPlot: MPlot) {
    this.db.object('plot/' + id).update(reworkPlot);
  }

  getSpotList(): Observable<any> {
    return this.db.list('parkingSpot').snapshotChanges();
  }

  getCurrentSpot(id: string): Observable<any> {
    return this.db.object('parkingSpot/' + id).snapshotChanges();
  }

  updateSpot(spot: MParkingSpot) {
    this.db.object('parkingSpot/' + spot.id).update(spot);
  }

  addTenantToDatabase(tenant: MTenant) {
    tenant.rentTo = null;
    tenant.rentFrom = null;
    tenant.price = null;
    this.db.object('tenant/' + tenant.id).set(tenant);
  }

  getTenants(): Observable<any> {
    return this.db.list('tenant').valueChanges();
  }

  getTenantsForUser(): Observable<any> {
    return this.db.list('tenant').valueChanges();
  }

  getCurrentPlot(uid: string): Observable<any> {
    return this.db.object('plot/' + uid).snapshotChanges();
  }

  deleteTenant(tenant: MTenant) {
    this.db.object('tenant/' + tenant.id).remove();
  }
}
