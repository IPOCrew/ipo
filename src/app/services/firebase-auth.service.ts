import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import {MUser} from '../models/mUser';
import {Observable, of, Subscription} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {AngularFireDatabase} from '@angular/fire/database';

class Credentials {
  email: string;
  password: string;
}

interface User {
  uid: string;
  email: string;
  displayName: string;
}

@Injectable({
  providedIn: 'root'
})
export class FirebaseAuthService {
  public user: MUser;
  public emailSent: boolean;
  public authState: any;
  public firebaseUser: Observable<User>;
  public subscription: Subscription;

  constructor(private fireAuth: AngularFireAuth, public database: AngularFireDatabase) {
    this.firebaseUser = this.fireAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.database.object('user/' + user.uid).valueChanges();
        } else {
          return of(null);
        }
      })
    );
  }

  public getAuthState() {
    return this.fireAuth.authState;
  }

  public createUser(credentials: Credentials): Promise<any> {
    return new Promise<any>((resolveIt, rejectIt) => {
      firebase.auth().createUserWithEmailAndPassword(credentials.email, credentials.password)
        .then(res => {
          firebase.auth().currentUser.sendEmailVerification();
          resolveIt(res);
        }, err => rejectIt(err));
    });
  }

  public signIn(credentials: Credentials): Promise<any> {
    return new Promise<any>((resolveIt, rejectIt) => {
      firebase.auth().signInWithEmailAndPassword(credentials.email, credentials.password)
        .then(res => {
          resolveIt(res);
        }, err => rejectIt(err));
    });
  }

  public signOut(): Promise<any> {
    return new Promise<any>((resolveIt, rejectIt) => {
      firebase.auth().signOut()
        .then( () => {
          this.firebaseUser = null;
        }, err => rejectIt(err));
    });
  }

  public getCurrentUserID(): string {
    return this.fireAuth.auth.currentUser.uid;
  }

  public getCurrentUser() {
    return this.fireAuth.auth.currentUser;
  }

  async sendPasswordReset(email: string): Promise<any> {
    return await new Promise<any>((resolveIt, rejectIt) => {
      firebase.auth().sendPasswordResetEmail(email)
        .then(res => {
          resolveIt(res);
        }, err => rejectIt(err));
    });
  }

  /**
   * Logs out the loggedIn User
   */
  public logOut(): Promise<any> {
    return new Promise<any>((resolveIt, rejectIt) => {
      firebase.auth().signOut()
        .then(res => {
          resolveIt(res);
        }, err => rejectIt(err));
    });
  }
}




