import { TestBed } from '@angular/core/testing';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseAuthService } from './firebase-auth.service';
import { BehaviorSubject } from 'rxjs';

const credetialsMock = {
  email: 'ipocrew.projekt@gmail.com',
  password: 'ipocrew123'
};

const userMock = {
  uid: 'ABC123',
  email: credetialsMock.email,
};

const fakeAuthState = new BehaviorSubject(null);

const fakeSignInHandler = (email, password): Promise<any> => {
  fakeAuthState.next(userMock);
  return Promise.resolve(userMock);
};

const fakeSignOutHandler = (): Promise<any> => {
  fakeAuthState.next(null);
  return Promise.resolve();
};

const angularFireAuthStub = {
  authState: fakeAuthState,
  auth: {
    createUserWithEmailAndPassword: jasmine
      .createSpy('createUserWithEmailAndPassword')
      .and
      .callFake(fakeSignInHandler),
    signInWithEmailAndPassword: jasmine
      .createSpy('signInWithEmailAndPassword')
      .and
      .callFake(fakeSignInHandler),
    signOut: jasmine
      .createSpy('signOut')
      .and
      .callFake(fakeSignOutHandler),
    currentUser: {
      sendEmailVerification: jasmine
        .createSpy('sendEmailVerification')
        .and
        .callFake(fakeSignInHandler),
    },
  },
};

describe('FirebaseAuthService', () => {
  let service: FirebaseAuthService;
  let afAuth: AngularFireAuth;
  // let isAuth$: Subscription;
  // let isAuthRef: boolean;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        FirebaseAuthService,
        { provide: AngularFireAuth, useValue: angularFireAuthStub },
      ],
    });

    service = TestBed.get(FirebaseAuthService);
    afAuth = TestBed.get(AngularFireAuth);
  });

  /**
   * Test: LogOut User
   */
  /*
  it('should signOut MUser', () => {
    fakeAuthState.next(userMock);
    service.logOut();
    expect(service.user).toBe(null);
  });
  */

  // beforeEach(() => {
  // isAuth$ = service.MUser.emailVerified;
  // });
  afterEach(() => {
    fakeAuthState.next(null);
  });

  /*
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should createUser', () => {
    service.createUser(credetialsMock);
    expect(afAuth.auth.createUserWithEmailAndPassword)
      .toHaveBeenCalledWith(credetialsMock.email, credetialsMock.password);
    expect(service.user.email).toBe(credetialsMock.email);
  });
  */
});
