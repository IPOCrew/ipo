export class FirebaseTestsHelper {

  static createFakePayload(objects: any[]): any {

    console.log(objects);
    const items = [];

    objects.forEach(obj => {
      items.push({
        payload: {
          val: function () {
            return obj;
          }
        }
      });
    });
    console.log(items);
    return items;
  }

  static createSingleItemFakePayload(object: any): any {
    let obj: any;
    obj = ({
      payload: {
        val: function () {
          return object;
        }
      }
    });
    return obj;
  }
}
