import {SweetAlertType} from 'sweetalert2';

export class InputHelper {

  public static hello = 'Hallo ';
  public static ups = 'Ups!';
  public static inputError = 'Bitte füllen Sie Ihre Eingaben vollständig aus!';
  public static emailError = 'Bitte geben Sie eine richtige E-mail ein!';
  public static nameError = 'Bitte geben Sie einen richtigen Namen ein!';
  public static shortMessage = 'Ihre Nachricht ist zu kurz!';
  public static messageSent = 'Ihre Nachricht wurde übermitelt';
  public static success: SweetAlertType = 'success';
  public static error: SweetAlertType = 'error';
  public static info: SweetAlertType = 'info';
  public static warning: SweetAlertType = 'warning';
  public static question: SweetAlertType = 'question';
  public static sent = 'Sent :)';

}
