import {Component} from '@angular/core';
import {FirebaseDatabaseService} from './services/firebase-database.service';
import {FirebaseAuthService} from './services/firebase-auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private firebaseService: FirebaseDatabaseService,
              private authService: FirebaseAuthService) {
  }
}
