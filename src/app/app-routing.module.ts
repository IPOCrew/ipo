import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {RegistrierungComponent} from './components/registrierung/registrierung.component';
import {MainPageComponent} from './components/main-page/main-page.component';
import {SinglePlotComponent} from './components/single-plot/single-plot.component';
import {LandingpageComponent} from './components/landingpage/landingpage.component';
import {CustomerManagementComponent} from './components/customer-management/customer-management.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'registration/:id',
    component: RegistrierungComponent
  },
  {
    path: 'main-page',
    component: MainPageComponent,
  },
  {
    path: 'parking-spot/:id',
    component: SinglePlotComponent,
  },
  {
    path: 'customer-management',
    component: CustomerManagementComponent,
  },
  {
    path: 'single-plot/:plotID',
    component: SinglePlotComponent,
  },
  {
    path: 'landingPage',
    component: LandingpageComponent
  },
  {
    path: '',
    redirectTo: 'landingPage',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
