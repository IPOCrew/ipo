import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MainPageComponent} from './main-page.component';
import {AppModule} from '../../app.module';
import {MPlot} from '../../models/mPlot';
import {FirebaseTestsHelper} from '../../helper/firebase.tests.helper';
import {MUser} from '../../models/mUser';
import {FirebaseDatabaseService} from 'src/app/services/firebase-database.service';
import {FirebaseAuthService} from 'src/app/services/firebase-auth.service';
import {Observable, of, Subscription} from 'rxjs';

describe('MainPageComponent', () => {
  let component: MainPageComponent;
  let fixture: ComponentFixture<MainPageComponent>;
  let databaseService: FirebaseDatabaseService;
  let authService: FirebaseAuthService;

  function fakePlotListCall() {

    spyOn(component.databaseService, 'getPlotList').and.callFake(() => {
        return {
          subscribe: (callback) => {
            const plot1 = new MPlot();
            plot1.userID = component.user.uid;
            callback(FirebaseTestsHelper.createFakePayload([plot1]));
          }
        };
      }
    );
  }

  function fakePlot() {
    component.newPlot = new MPlot();
    component.newPlot.id = 'aa';
    component.newPlot.address = 'teststraße5';
    component.newPlot.plotPKW = 5;
    component.newPlot.plotLKW = 2;
    component.newPlot.userID = component.user;
    component.newPlot.name = 'test';
  }

  function fakeUserCall() {
    spyOn(component.authService, 'getCurrentUserID').and.callFake(() => {
      return 'ABC123';
    });
    spyOn(component.databaseService, 'getCurrentUser').and.callFake(() => {
      return {
        subscribe: (callback) => {
          const user = new MUser();
          user.email = 'pascalf.neusta@gmail.com';
          user.company = 'nms';
          user.lastName = 'freund';
          user.firstName = 'pascal';
          user.id = 'ABC123';
          callback(FirebaseTestsHelper.createSingleItemFakePayload(user));
        }
      };
    });
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPageComponent);
    component = fixture.componentInstance;
    fakeUserCall();
    fixture.detectChanges();
    databaseService = fixture.debugElement.injector.get(FirebaseDatabaseService);
    authService = fixture.debugElement.injector.get(FirebaseAuthService);
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


  /*
  it('should get current user from service', () => {

    expect(component.authService.getCurrentUserID).toHaveBeenCalled();
    expect(component.user).toBeTruthy();
  });
  */


  it('should call addPlot from service', () => {
    component.user = 'id';
    component.newPlot = new MPlot();
    component.newPlot.id = 'aa';
    component.newPlot.address = 'teststraße5';
    component.newPlot.plotPKW = 5;
    component.newPlot.plotLKW = 2;
    component.newPlot.userID = component.user;
    component.newPlot.name = 'test';
    const spy = spyOn(component.databaseService, 'addPlotToDatabase').and.callFake(() => {
    });
    spyOn(component.databaseService, 'addParkingSpotToDatabase').and.callFake(() => {

    });
    component.addPlot();
    expect(spy).toHaveBeenCalled();
  });

  it('should not create a plot if input is wrong', () => {
    spyOn(component.databaseService, 'addParkingSpotToDatabase').and.callFake(() => {

    });
    component.user = 'id';
    component.newPlot = new MPlot();
    component.newPlot.id = 'aa';
    component.newPlot.address = 'teststraße5';
    component.newPlot.plotPKW = 5;
    component.newPlot.plotLKW = 2;
    component.newPlot.userID = component.user;
    component.newPlot.name = undefined;
    component.addPlot();
  });

  it('should detect max range of PKW', () => {
    const dbSpy = spyOn(databaseService, 'addPlotToDatabase').and.callFake(() => {
    });
    spyOn(component.databaseService, 'addParkingSpotToDatabase').and.callFake(() => {

    });
    component.user = 'id';
    component.newPlot = new MPlot();
    component.newPlot.id = 'aa';
    component.newPlot.address = 'teststraße5';
    component.newPlot.plotPKW = 501;
    component.newPlot.plotLKW = 2;
    component.newPlot.userID = component.user;
    component.newPlot.name = 'test';
    component.addPlot();
    // Cant test because it will be reseted!
    // expect(component.newPlot.plotPKW).toBe(500);
  });

  it('should detect min range of PKW', () => {
    const dbSpy = spyOn(databaseService, 'addPlotToDatabase').and.callFake(() => {
    });
    spyOn(component.databaseService, 'addParkingSpotToDatabase').and.callFake(() => {

    });
    component.user = 'id';
    component.newPlot = new MPlot();
    component.newPlot.id = 'aa';
    component.newPlot.address = 'teststraße5';
    component.newPlot.plotPKW = -3;
    component.newPlot.plotLKW = 2;
    component.newPlot.userID = component.user;
    component.newPlot.name = 'test';
    component.addPlot();
    // Cant test because it will be reseted!
    // expect(component.newPlot.plotPKW).toBe(500);
  });


  it('should detect max range of LKW', () => {
    const dbSpy = spyOn(databaseService, 'addPlotToDatabase').and.callFake(() => {
    });
    spyOn(component.databaseService, 'addParkingSpotToDatabase').and.callFake(() => {

    });
    component.user = 'id';
    component.newPlot = new MPlot();
    component.newPlot.id = 'aa';
    component.newPlot.address = 'teststraße5';
    component.newPlot.plotPKW = 5;
    component.newPlot.plotLKW = 501;
    component.newPlot.userID = component.user;
    component.newPlot.name = 'test';
    component.addPlot();
    // Cant test because it will be reseted!
    // expect(component.newPlot.plotPKW).toBe(500);
  });

  it('should detect min range of PKW', () => {
    const dbSpy = spyOn(databaseService, 'addPlotToDatabase').and.callFake(() => {
    });
    spyOn(component.databaseService, 'addParkingSpotToDatabase').and.callFake(() => {

    });
    component.user = 'id';
    component.newPlot = new MPlot();
    component.newPlot.id = 'aa';
    component.newPlot.address = 'teststraße5';
    component.newPlot.plotPKW = 2;
    component.newPlot.plotLKW = -5;
    component.newPlot.userID = component.user;
    component.newPlot.name = 'test';
    component.addPlot();
    //Cant test because it will be reseted!
    //expect(component.newPlot.plotPKW).toBe(500);
  });

  it('should logout', () => {
    const authSpy = spyOn(authService, 'logOut').and.callFake(() => {
    });
    spyOn(component.databaseService, 'addParkingSpotToDatabase').and.callFake(() => {

    });
    component.logOut();
    expect(authSpy).toHaveBeenCalled();
  });

  it('should destroy', () => {
    component.plotSubscription = new Subscription(() => {
    });
    spyOn(component.databaseService, 'addParkingSpotToDatabase').and.callFake(() => {

    });
    component.ngOnDestroy();
    //expect(component.plotSubscription).toBe(null);
  });

  it('should delete plots for user', () => {
    spyOn(component.databaseService, 'addParkingSpotToDatabase').and.callFake(() => {

    });
    const plot: MPlot = new MPlot();
    plot.userID = '1';
    component.user = {uid: '1'};
    const item = {
      payload: {
        val() {
          return plot;
        }
      }
    };
    const spy = spyOn(databaseService, 'getPlotList').and.returnValue(of([item]));
    component.userPlots = [];
    component.getPlotsForUser();
    expect(component.userPlots[0]).toEqual(plot);

  });

  it('should delete a given Plot', () => {
    spyOn(component.databaseService, 'addParkingSpotToDatabase').and.callFake(() => {

    });
    const item = {
      payload: {
        val() {
          return {plotID: '1'};
        }
      }
    };
    const spy = spyOn(databaseService, 'getPlotList').and.returnValue(of([item]));
    const deleteSpy = spyOn(databaseService, 'deleteParkingSpotFromDatabase').and.callFake(() => {
    });
    const deletePlotSpy = spyOn(databaseService, 'deletePlotFromDatabase').and.callFake(() => {
    });
    // Just to be sure that it can be its own instance
    const deletingPlot: MPlot = new MPlot();
    deletingPlot.id = '1';
    component.deletePlot(deletingPlot);
    //expect(deleteSpy).toHaveBeenCalled();
  });

  it('should call getPlotList from service', () => {
    fakePlotListCall();
    spyOn(component, 'getPlotsForUser').and.callFake(() => {

    });
    component.getPlotsForUser();
    expect(component.getPlotsForUser).toHaveBeenCalled();
  });


  /*
  it('should get specific user plots', () => {
    fakePlotListCall();
    component.getPlotsForUser();
    expect(component.userPlots[0]).toBeDefined();
    expect(component.userPlots[0].userID).toEqual(component.user);
    expect(component.userPlots[1]).toBeUndefined();
  });

  it('should call add ParkingSpots from service', () => {
    spyOn(component.databaseService, 'addParkingSpotToDatabase').and.callFake(() => {
    });
    fakePlot();
    component.addPlot();
    expect(component.databaseService.addParkingSpotToDatabase).toHaveBeenCalled();
    expect(component.databaseService.addParkingSpotToDatabase).toHaveBeenCalledTimes(7);
  });
  */

});
