import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FirebaseAuthService} from '../../services/firebase-auth.service';
import {FirebaseDatabaseService} from '../../services/firebase-database.service';
import {MPlot} from '../../models/mPlot';
import {v4 as uuid} from 'uuid';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MParkingSpot} from '../../models/mParkingSpot';
import {Router} from '@angular/router';
import {MTenant} from '../../models/mTenant';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css'],
})
export class MainPageComponent implements OnInit, OnDestroy {

  constructor(public authService: FirebaseAuthService,
              public databaseService: FirebaseDatabaseService,
              private modalService: NgbModal,
              public router: Router) {
  }

  search: string;
  user: any;
  newPlot: MPlot = new MPlot();
  userPlots: MPlot[] = [];
  parkingSpot: MParkingSpot;
  plotSubscription: Subscription;

  @ViewChild('content') modal: any;

  ngOnInit() {
    this.authService.getAuthState().first().subscribe(user => {
      if (user) {
        console.log(user);
        this.user = user;
        this.getPlotsForUser();
      } else {
        this.router.navigate(['login']);
      }
    });
  }

  addPlot() {
    if (this.newPlot.name === undefined ||
      this.newPlot.name === '' ||
      this.newPlot.address === undefined ||
      this.newPlot.address === '' ||
      this.newPlot.plotPKW === undefined ||
      this.newPlot.plotPKW === null ||
      this.newPlot.plotLKW === undefined ||
      this.newPlot.plotLKW === null) {
      return;
    }

    if (this.newPlot.plotPKW > 500) {
      this.newPlot.plotPKW = 500;
    } else if (this.newPlot.plotPKW < 0) {
      this.newPlot.plotPKW = 0;
    }

    if (this.newPlot.plotLKW > 500) {
      this.newPlot.plotLKW = 500;
    } else if (this.newPlot.plotLKW < 0) {
      this.newPlot.plotLKW = 0;
    }

    let i: number;
    this.newPlot.id = uuid();
    this.newPlot.userID = this.user.uid;
    this.databaseService.addPlotToDatabase(this.newPlot);

    for (i = 0; i < this.newPlot.plotPKW; i++) {
      const t = new MTenant('', '', '', '', '', '', '', '',  '', '', 0);
      this.parkingSpot = new MParkingSpot(uuid(),
        this.newPlot.id,
        'Car',
        false,
        '',
        [t],
        '',
        '',
        '',
        0);
      this.databaseService.addParkingSpotToDatabase(this.parkingSpot);
    }

    for (i = 0; i < this.newPlot.plotLKW; i++) {
      let t: MTenant;
      t = new MTenant('', '', '', '', '', '', '', '',  '', '', 0);
      this.parkingSpot = new MParkingSpot(uuid(),
        this.newPlot.id,
        'Truck',
        false,
        '',
        [t],
        '',
        '',
        '',
        0);
      this.databaseService.addParkingSpotToDatabase(this.parkingSpot);

    }

    this.newPlot = new MPlot();
    this.closeModal();
  }

  deletePlot(plot: MPlot) {
    this.databaseService.getSpotList().subscribe((list) => {
      list.forEach((item) => {
        if (item.payload.val().plotID === plot.id) {
          this.databaseService.deleteParkingSpotFromDatabase(item.payload.val());
        }
      });
      this.databaseService.deletePlotFromDatabase(plot);
    });
  }

  getPlotsForUser() {
    this.plotSubscription = this.databaseService.getPlotList().subscribe((list) => {
      this.userPlots = [];
      list.forEach((item) => {
        console.log(item.payload.val());
        if (item.payload.val().userID === this.user.uid) {
          this.userPlots.push(item.payload.val());
        }
      });
    });
  }

  navigatePlot(id: string) {
    this.databaseService.setCurrentPlot(id);
    this.router.navigate(['parking-spot', id]);
  }

  openCustomerManagement() {
    this.router.navigate(['customer-management']);
  }

  openModal(content) {
    this.modalService.open(content);
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  ngOnDestroy(): void {
    if (this.plotSubscription) {
      this.plotSubscription.unsubscribe();
    }
  }

  logOut() {
    this.authService.logOut();
    console.log(this.user);
    this.router.navigate(['landingPage']);
  }
}
