import { Component, OnInit, ViewChild } from '@angular/core';
import { FirebaseAuthService } from 'src/app/services/firebase-auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

class Credentials {
  email: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  credentials: Credentials = new Credentials();
  email: string;
  password: string;


  @ViewChild('content') modal: any;

  constructor(public authService: FirebaseAuthService,
    public modalService: NgbModal,
    public router: Router) {
  }

  user: any;
  resetEmail: string;
  sent: boolean;
  price: number;

  ngOnInit() {
    this.email = '';
    this.password = '';
    const observer = this.authService.getAuthState();
    if (observer !== undefined) {
      observer.first().subscribe(user => {
        if (user) {
          this.router.navigate(['main-page']);
        } else {
          console.log('not logged in');
        }
      });
    }

  }

  async logIn() {
    this.credentials.email = this.email;
    this.credentials.password = this.password;
    this.authService.signIn(this.credentials)
      .then(() => {
        this.router.navigate(['main-page']);
      }, err => {
        console.log('ERROR');
        console.log(err);

        switch (err.code) {
          case 'auth/invalid-email':
            this.popup('UFFFF...', 'You need a valid E-mail!');
            break;
          case 'auth/wrong-password':
            this.popup('HMMMM...', 'This is not your password!');
            break;
          case 'auth/user-not-found':
            this.popup('PEW PEW...', 'Did you register yet? I can\'t find you! :(');
            break;
        }
      });
  }

  popup(title: string, reason: string) {
    Swal.fire(title, reason, 'info');
  }

  navigate() {
    this.router.navigate(['registration']);
  }

  async sendPasswordResetMail() {
    try {
      await this.authService.sendPasswordReset(this.resetEmail);
      this.popup('YUHUUU', 'We have sent you an E-mail!');
    } catch (e) {
      console.log(e);
      switch (e.code) {
        case 'auth/argument-error':
          this.popup('UFFFF...', 'You need a valid E-mail!');
          break;
        case 'auth/invalid-email':
          this.popup('HMMMM...', 'You need a valid E-mail!');
          break;
        case 'auth/user-not-found':
          this.popup('PEW PEW...', 'Did you register yet? I can\'t find you! :(');
          break;
      }
    }
  }


  openModal(content) {
    this.modalService.open(content);
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  openPasswordForgottenBox() {
    document.getElementById('0').style.display = 'none';
    document.getElementById('1').style.display = 'flex';
  }

  closePasswordForgottenBox() {
    document.getElementById('1').style.display = 'none';
    document.getElementById('0').style.display = 'flex';
  }
}
