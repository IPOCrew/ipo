import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppModule } from 'src/app/app.module';
import { LoginComponent } from './login.component';
import { url } from 'inspector';
import { RouterTestingModule } from '@angular/router/testing';
import { FirebaseAuthService } from 'src/app/services/firebase-auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { throwError, Subscriber, Observable } from 'rxjs';
import { throws } from 'assert';
import { ObserveOnMessage } from 'rxjs/internal/operators/observeOn';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: FirebaseAuthService;
  let router: Router;
  let modalService: NgbModal;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      providers: [FirebaseAuthService, NgbModal]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    //Create a fake sign in so we do not run into errors
    router = TestBed.get(Router);
    modalService = TestBed.get(NgbModal);
    //Dont navigate
    spyOn(router, 'navigate').and.callFake(() => { });
    authService = TestBed.get(FirebaseAuthService);
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    fixture.ngZone.run(() => {
      expect(component).toBeTruthy();
    });
  });


  it('should login', () => {
    const spy = spyOn(authService, 'signIn').and.returnValue(Promise.resolve(true));
    component.logIn();
    expect(spy).toHaveBeenCalled();
  });

  it('thould throw on invalid mail login', () => {
    const spy = spyOn(authService, 'signIn').and.returnValue(Promise.reject({ code: 'auth/invalid-email' }));
    component.logIn();
    expect(spy).toHaveBeenCalled();
  });

  it('thould throw on invalid password login', () => {
    const spy = spyOn(authService, 'signIn').and.returnValue(Promise.reject({ code: 'auth/wrong-password' }));
    component.logIn();
    expect(spy).toHaveBeenCalled();
  });

  it('thould throw on user not found login', () => {
    const spy = spyOn(authService, 'signIn').and.returnValue(Promise.reject({ code: 'auth/user-not-found' }));
    component.logIn();
    expect(spy).toHaveBeenCalled();
  });

  it('should navigate to registration', () => {
    fixture.ngZone.run(() => {
      component.navigate();
    });
  });

  it('should send a password reset mail', () => {
    const spy = spyOn(authService, 'sendPasswordReset').and.returnValue('success');
    component.sendPasswordResetMail();
    expect(spy).toHaveBeenCalled();
  });

  it('should handle password reset exceptions auth/argument-error', () => {
    const spy = spyOn(authService, 'sendPasswordReset').and.callFake(() => { throw { code: 'auth/argument-error' }; });
    component.sendPasswordResetMail();
    expect(spy).toHaveBeenCalled();
  });

  it('should handle password reset exceptions auth/invalid-email', () => {
    const spy = spyOn(authService, 'sendPasswordReset').and.callFake(() => { throw { code: 'auth/invalid-email' }; });
    component.sendPasswordResetMail();
    expect(spy).toHaveBeenCalled();
  });

  it('should handle password reset exceptions auth/user-not-found', () => {
    const spy = spyOn(authService, 'sendPasswordReset').and.callFake(() => { throw { code: 'auth/user-not-found' }; });
    component.sendPasswordResetMail();
    expect(spy).toHaveBeenCalled();
  });

  it('should opan modal', () => {
    const spy = spyOn(modalService, 'open').and.callFake(() => { });
    component.openModal('');
    expect(spy).toHaveBeenCalled();
  });

  it('should close modal', () => {
    const spy = spyOn(modalService, 'dismissAll').and.callFake(() => { });
    component.closeModal();
    expect(spy).toHaveBeenCalled();
  });

  it('should open password forgot box', () => {
    component.openPasswordForgottenBox();
  });

  it('should close password forgot box', () => {
    component.closePasswordForgottenBox();
  });
  /*
  it('should have credentials', () => {
    expect(component.credentials).toBeTruthy();
  });

  it('should have service', () => {
    expect(component.authService).toBeTruthy();
  });

  it('should call signIn from service', () => {
    fakeSignIn();
    component.logIn();
    expect(component.authService.signIn).toHaveBeenCalled();
  });
  */
  /*
   it('fields cant be empty', async () => {
     
     spyOn(component.authService, 'getCurrentUser').and.callFake(() => {
       return true;
     });
     fakeSignIn();
     await component.logIn();
     expect(component.router.url).toEqual('/user-profile');
   });
 
   it('should get currentUser from service', async () => {
     spyOn(component.authService, 'getCurrentUser').and.callFake(() => {
     });
     fakeSignIn();
     await component.logIn();
     expect(component.authService.getCurrentUser).toHaveBeenCalled();
   });
 
   it('should call sendPasswordReset from service', () => {
     spyOn(component.authService, 'sendPasswordReset');
     component.sendPasswordResetMail();
     expect(component.authService.sendPasswordReset).toHaveBeenCalled();
   });
 
   it('should get a console.log', () => {
     spyOn(console, 'log');
     component.email = '';
     component.password = '';
     component.logIn();
     expect(console.log).toHaveBeenCalled();
   });
 
   it('should open PasswordForgottenBox', () => {
     const dummyElement = document.createElement('div');
     spyOn(document, 'getElementById').and.returnValue(dummyElement);
     component.openPasswordForgottenBox();
     expect(document.getElementById).toHaveBeenCalled();
   });
 
   it('should close PasswordForgottenBox', () => {
     const dummyElement = document.createElement('div');
     spyOn(document, 'getElementById').and.returnValue(dummyElement);
     component.closePasswordForgottenBox();
     expect(document.getElementById).toHaveBeenCalled();
   });
 
   it('should dismiss modal', () => {
     spyOn(component.modalService, 'dismissAll');
     component.closeModal();
     expect(component.modalService.dismissAll).toHaveBeenCalled();
   });
   */

});
