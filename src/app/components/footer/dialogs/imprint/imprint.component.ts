import {Component} from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-imprint',
  templateUrl: './imprint.component.html',
  styleUrls: ['./imprint.component.css']
})
export class ImprintComponent {
  constructor() {
  }

  showImprintDialog() {
    Swal.fire({
      width: 900,
      title: 'Impressum',
      html: '<div style="text-align: center"><strong >Intelligent Plot Organisation GmbH</strong>' +
        '<br><strong>Europaschulestr.10</strong>' +
        '<br><strong>28217 Bremen</strong>' +
        '<br><strong>ipo.it@ipo.de</strong></div><br>',
      showConfirmButton: false,
      showCloseButton: true,
      focusCancel: false
    });
  }
}
