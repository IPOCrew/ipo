import {Component} from '@angular/core';
import {FirebaseDatabaseService} from '../../../../services/firebase-database.service';
import {MContact} from '../../../../models/mContact';
import Swal, {SweetAlertType} from 'sweetalert2';
import {InputHelper} from '../../../../helper/input.helper';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})

export class ContactComponent {
  contactName = '';
  contactEmail = '';
  contactMessage = '';
  contactTime = '';
  sendClicked = false;

  constructor(private  dbService: FirebaseDatabaseService) {
  }

  async onContactSend() {
    this.contactTime = Date.now().toString();
    if (this.contactName !== '' && this.contactEmail !== '' && this.contactMessage !== '') {
      if (this.contactName.trim().length < 3) {
        this.showInfoAlert(InputHelper.hello + this.contactName, InputHelper.nameError, InputHelper.error);
      } else if (!this.contactEmail.includes('@') && !this.contactEmail.includes('.')) {
        this.showInfoAlert(InputHelper.hello + this.contactName, InputHelper.emailError, InputHelper.error);
      } else if (this.contactMessage.trim().length < 10) {
        this.showInfoAlert(InputHelper.hello + this.contactName, InputHelper.shortMessage, InputHelper.warning);
      } else {
        await this.dbService.addContactToDatabase(new MContact(this.contactName,
          this.contactEmail, this.contactMessage,
          this.contactTime));
        console.log(this.contactTime);
        this.sendClicked = true;
        this.showInfoAlert(InputHelper.sent, InputHelper.messageSent, InputHelper.success);
      }
    } else {
      this.showInfoAlert(InputHelper.ups, InputHelper.inputError, InputHelper.error);
      console.log(this.contactMessage);
    }

  }

  showInfoAlert(title: string, message: string, type: SweetAlertType) {
    Swal.fire({
      title: title,
      text: message,
      type: type,
      confirmButtonColor: '#d4ac0d',

    });
  }
}

