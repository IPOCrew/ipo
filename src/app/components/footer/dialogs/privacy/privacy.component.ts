import {Component} from '@angular/core';
import Swal from 'sweetalert2';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent {
  constructor() {
  }

  privacyRequest() {
    Swal.fire({
      title: 'Privacy Policy',
      width: 900,
      allowOutsideClick: false,
      confirmButtonColor: '#d4ac0d',
      html: '<div style="text-align: center">This is a privacy policy. We take your privacy very serious.' +
        'If you want any of your private information to not be stored or to be deleted, you may contact us. :)',
      showConfirmButton: true,
      confirmButtonText: 'Accept',
      focusConfirm: true,
    }).then((result) => {
    });
  }

  thanksAlert() {
    Swal.fire({
      title: 'Thanks!',
      type: 'success',
      allowOutsideClick: false,
      timer: 1400,
      showConfirmButton: false
    });
  }
}


