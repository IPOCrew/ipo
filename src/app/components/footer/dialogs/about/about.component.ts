import {Component} from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent {
  constructor() {
  }

  showAboutUsDialog() {
    document.getElementById('about');
    Swal.fire({
      width: 800,
      title: 'What is IPO?',
      confirmButtonText: 'Dismiss',
      allowOutsideClick: false,
      focusConfirm: false,
      confirmButtonColor: '#d4ac0d',
      html:
        '<div style="text-align: center"> IPO is a simple way to manage and rent parking lots. ' +
        'IPO stands for "Intelligent Parking Organisation and is simple to use.' +
        'The project was started by six studens as a school project <br>' +
        'and is meanwhile one of the biggest and best solutions if it' +
        'comes to managing your parking properties.<br> ' +
        '<br><strong>Our goal is to make your life simpler and more easy, we hope you enjoy!</strong></div> ',
    });
  }

}
