import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AlertComponent} from './alert.component';
import {AppModule} from 'src/app/app.module';
import {MatDialogRef, MatDialogModule} from '@angular/material';
import {MatButtonModule, MatIconModule, MatTreeModule} from '@angular/material';

describe('AlertComponent', () => {
  let component: AlertComponent;
  let fixture: ComponentFixture<AlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, MatButtonModule, MatIconModule, MatTreeModule],
      declarations: [],
      providers: [{provide: MatDialogRef, useValue: {}}, ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
