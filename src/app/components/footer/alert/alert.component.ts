import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {ContactComponent} from '../dialogs/contact/contact.component';
import {debounceTime} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})

export class AlertComponent implements OnInit {
  @ViewChild('alert') alert: ElementRef;

  constructor(private dialog: MatDialogRef<ContactComponent>) {
  }

  private _success = new Subject<string>();
  successMessage: string;

  ngOnInit(): void {
    this._success.subscribe((message) => this.successMessage = message);
    this._success.pipe(
      debounceTime(5000)
    ).subscribe(() => {
      this.successMessage = null;
      this.closeAlert();
    });
    this.successMessageSent();
  }

  public successMessageSent() {
    this._success.next(`Ihre Nachricht wurde übermittelt.`);
  }

  closeAlert() {
    this.dialog.close();
  }

}
