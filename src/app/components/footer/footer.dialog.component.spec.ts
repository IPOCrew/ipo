import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FooterDialogComponent } from './footer.dialog.component';
import { AppModule } from 'src/app/app.module';
import { MatDialogModule } from '@angular/material';

describe('Footer.DialogComponent', () => {
  let component: FooterDialogComponent;
  let fixture: ComponentFixture<FooterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, MatDialogModule],
      declarations: []
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set color', () => {
    component.position = 5;
    const val = component.setColor();
    expect(val).toBe('0');
  });

  it('should set color over a value of 15', () => {
    component.position = 20;
    const val = component.setColor();
    expect(val).toBe('0');
  });

  it('should fire onSCroll', () => {
    const val = component.onWindowScroll(5);
    //The setColor sets the value to zero!
    expect(val).toBe(0);
  });

  //For Code Coverage
  it('should open a dialog with id 1', () => {
    component.openDialog(1);
  });
  it('should open a dialog with id 2', () => {
    component.openDialog(2);
  });
  it('should open a dialog with id 3', () => {
    component.openDialog(3);
  });
  it('should open a dialog with id 4', () => {
    component.openDialog(4);
  });

});
