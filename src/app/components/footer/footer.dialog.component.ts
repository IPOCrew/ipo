import {Component, HostListener} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {PrivacyComponent} from './dialogs/privacy/privacy.component';
import {ContactComponent} from './dialogs/contact/contact.component';
import {ImprintComponent} from './dialogs/imprint/imprint.component';
import {AboutComponent} from './dialogs/about/about.component';

@Component({
  selector: 'app-footer-dialog',
  templateUrl: './footer.dialog.component.html',
  styleUrls: ['./footer.dialog.component.css'],
})
export class FooterDialogComponent {
  footerColor: String = '#fdfdfe';
  position: number;
  readonly PRIVACY_ID: number;
  readonly IMPRINT_ID: number;
  readonly CONTACT_ID: number;
  readonly ABOUT_ID: number;


  constructor(private fb: FormBuilder,
              private dialog: MatDialog) {
    this.PRIVACY_ID = 1;
    this.IMPRINT_ID = 2;
    this.CONTACT_ID = 3;
    this.ABOUT_ID = 4;
  }

  @HostListener('window:scroll', [])
  onWindowScroll(position: number) {
    position = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    this.position = position;
    this.setColor();
    return position;
  }


  setColor() {
    if (this.position > 15) {
      this.footerColor = '0';
    } else {
      this.footerColor = '0';
    }
    return this.footerColor;
  }

  openDialog(clickID: number) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    switch (clickID) {
      case this.PRIVACY_ID: {
        const privacy = new PrivacyComponent();
        privacy.privacyRequest();
        //this.dialog.open(PrivacyComponent, dialogConfig);
        console.log('Datenschutz');
        break;
      }
      case this.IMPRINT_ID: {
        const imprint = new ImprintComponent();
        imprint.showImprintDialog();
        //this.dialog.open(ImprintComponent, dialogConfig);
        console.log('Impressum');
        break;
      }
      case this.CONTACT_ID: {
        this.dialog.open(ContactComponent, dialogConfig);
        console.log('Kontakt');
        break;
      }
      case this.ABOUT_ID: {
        const about = new AboutComponent();
        about.showAboutUsDialog();

        //this.dialog.open(AboutComponent, dialogConfig);
        console.log('Über uns');
        break;
      }
    }
  }
}
