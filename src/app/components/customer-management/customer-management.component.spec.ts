import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerManagementComponent } from './customer-management.component';
import { AppModule } from 'src/app/app.module';
import { FirebaseDatabaseService } from 'src/app/services/firebase-database.service';
import { of } from 'rxjs';
import { MTenant } from 'src/app/models/mTenant';
import { FirebaseAuthService } from 'src/app/services/firebase-auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

describe('CustomerManagementComponent', () => {
  let component: CustomerManagementComponent;
  let fixture: ComponentFixture<CustomerManagementComponent>;
  let databaseService: FirebaseDatabaseService;
  let authService: FirebaseAuthService;
  let modalService: NgbModal;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [],
      providers: [FirebaseDatabaseService, FirebaseAuthService, NgbModal]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    databaseService = fixture.debugElement.injector.get(FirebaseDatabaseService);
    authService = fixture.debugElement.injector.get(FirebaseAuthService);
    modalService = fixture.debugElement.injector.get(NgbModal);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load subscribe', () => {
    const spy = spyOn(databaseService, 'getTenants').and.returnValue(of(['']));
    const fakeLoad = spyOn(component, 'loadTenantsForUser').and.returnValue(null);
    component.loadSubscribe();
    expect(spy).toHaveBeenCalled();
  });

  it('should load tenants for user', () => {
    const a = new MTenant();
    component.user = a;
    component.allTenants.push(a);
    component.userTenants = [];
    component.loadTenantsForUser();
    expect(component.userTenants[0]).toBe(a);
  });

  it('should change tenant', () => {
    const spy = spyOn(component, 'closeModal').and.callThrough();
    const databaseSpy = spyOn(databaseService, 'addTenantToDatabase').and.returnValue(null);
    component.changeTenant();
    expect(spy).toHaveBeenCalled();
  });

  it('should delete tenant', () => {
    const spy = spyOn(component, 'closeModal').and.callThrough();
    const databaseSpy = spyOn(databaseService, 'deleteTenant').and.returnValue(null);
    component.deleteTenant(new MTenant());
    expect(spy).toHaveBeenCalled();
  });

  it('should create uuid if not given', () => {
    const databaseSpy = spyOn(databaseService, 'addTenantToDatabase').and.returnValue(null);
    const t = new MTenant();
    t.userId = 'userid';
    t.id = undefined;
    const val = component.addTenant(t);
    expect(t === undefined).toBe(false);
  });

  it('should set the user to current user if undefined', () => {
    const databaseSpy = spyOn(databaseService, 'addTenantToDatabase').and.returnValue(null);
    const t = new MTenant();
    component.user = { uid: 'userid' };
    t.userId = undefined;
    t.id = undefined;
    const val = component.addTenant(t);
    expect(t === undefined).toBe(false);
  });

  it('should alert if tenant id is not undefined', () => {
    const t = new MTenant();
    t.userId = 'userid';
    t.id = 't_id';
    const val = component.addTenant(t);
    expect(component.newTenant === undefined).toBe(false);
  });

  it('should logout', () => {
    fixture.ngZone.run(() => {
      const spy = spyOn(authService, 'signOut').and.callFake(() => { });
      component.logOut();
    });
  });

  it('should open a tenant Modal', () => {
    const tenant = new MTenant();
    const spy = spyOn(modalService, 'open').and.returnValue(null);
    component.openModal('content', tenant);
    expect(component.tenantToChange).toBe(tenant);
  });
});
