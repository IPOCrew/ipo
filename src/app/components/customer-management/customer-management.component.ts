import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FirebaseAuthService} from '../../services/firebase-auth.service';
import {FirebaseDatabaseService} from '../../services/firebase-database.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {v4 as uuid} from 'uuid';
import {MTenant} from '../../models/mTenant';

@Component({
  selector: 'app-customer-management',
  templateUrl: './customer-management.component.html',
  styleUrls: ['./customer-management.component.css']
})
export class CustomerManagementComponent implements OnInit, OnDestroy {

  user: any;
  allTenants: MTenant[] = [];
  tenantSubscription: Subscription;
  userTenants: MTenant[] = [];
  tenantToChange: MTenant = new MTenant();
  newTenant: MTenant = new MTenant();

  constructor(public authService: FirebaseAuthService,
              public databaseService: FirebaseDatabaseService,
              private modalService: NgbModal,
              public router: Router) {
  }

  @ViewChild('tenantModal') tenantModal: any;

  ngOnInit() {
    this.authService.getAuthState().first().subscribe(user => {
      if (user) {
        this.user = user;
      } else {
        this.router.navigate(['login']);
      }
    });
    this.loadSubscribe();
  }

  loadSubscribe() {
    this.tenantSubscription = this.databaseService.getTenants().subscribe(res => {
      this.allTenants = res;
      console.log(res);
      this.loadTenantsForUser();
    });
  }

  loadTenantsForUser() {
    this.userTenants = [];
    this.allTenants.forEach((tenant) => {
      if (tenant.userId === this.user.uid) {
        this.userTenants.push(tenant);
      }
    });
  }

  changeTenant() {
    this.databaseService.addTenantToDatabase(this.tenantToChange);
    this.closeModal();
  }

  deleteTenant(tenant: MTenant) {
    this.databaseService.deleteTenant(tenant);
    this.closeModal();
  }

  async addTenant(tenant: MTenant) {
    tenant = this.checkForTenantName(tenant);
    if (tenant.id !== undefined) {
      alert(tenant.name + ' aus ' + tenant.adress + ' existiert bereits in der Datenbank');
      this.newTenant = new MTenant();
      this.closeModal();
    } else {
      if (tenant.id === undefined) {
        tenant.id = uuid();
      }
      if (tenant.userId === undefined) {
        tenant.userId = this.user.uid;
      }
      this.databaseService.addTenantToDatabase(tenant);
      this.newTenant = new MTenant();
      this.closeModal();
    }
  }

  checkForTenantName(tenant: MTenant): MTenant {
    let tenantToReturn: MTenant = new MTenant();
    let gotUser = false;
    if (this.userTenants.length > 0) {
      this.userTenants.forEach((tenantInList) => {
        console.log('tenant to return: ' + tenantToReturn.id);
        if (tenantInList.name === tenant.name && tenantInList.adress === tenant.adress && tenantInList.userId === this.user.uid) {
          if (!gotUser) {
            tenantToReturn = tenantInList;
            gotUser = true;
          }
        } else {
          if (!gotUser) {
            tenantToReturn = tenant;
          }
        }
      });
      return tenantToReturn;
    } else {
      return tenant;
    }
  }


  openModal(content, tenant: MTenant) {
    console.log(tenant);
    this.tenantToChange = tenant;
    this.modalService.open(content);
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  ngOnDestroy(): void {
    this.tenantSubscription.unsubscribe();
  }

  logOut() {
    this.authService.logOut();
    this.router.navigate(['landingPage']);
  }

}
