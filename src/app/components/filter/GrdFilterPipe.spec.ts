import {GrdFilterPipe} from './GrdFilterPipe';

describe('GrdFilterPipe', () => {
  it('should create an instance', () => {
    expect(new GrdFilterPipe()).toBeTruthy();
  });

  it('should return items when no filter is given', () => {
    const filter = new GrdFilterPipe();
    const items = ['item'];
    const val = filter.transform(items, null, true);
    expect(val).toBe(items);
  });

  it('should return items when its not an array', () => {
    const filter = new GrdFilterPipe();
    const items = null;
    const val = filter.transform(items, 'filter', true);
    expect(val).toBe(items);
  });

  it('should return  empty if filter doesnt match', () => {
    const filter = new GrdFilterPipe();
    const items = ['item'];
    const val = filter.transform(items, 'filter', true);
    expect(val.length).toBe(0);
  });

  it('should return items if filter is not default', () => {
    const filter = new GrdFilterPipe();
    const items = ['item'];
    const val = filter.transform(items, 'filter', false);
    expect(val).toEqual(items);
  });
})
