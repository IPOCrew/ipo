import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglePlotComponent } from './single-plot.component';
import { AppModule } from '../../app.module';
import { MParkingSpot } from '../../models/mParkingSpot';
import { FirebaseTestsHelper } from '../../helper/firebase.tests.helper';
import { MPlot } from '../../models/mPlot';

describe('SinglePlotComponent', () => {
  let component: SinglePlotComponent;
  let fixture: ComponentFixture<SinglePlotComponent>;

  function fakeSpotListCall() {
    spyOn(component.databaseService, 'getSpotList').and.callFake(() => {
      return {
        subscribe: (callback) => {
          const spot1 = new MParkingSpot();
          const spot2 = new MParkingSpot();
          spot1.plotID = 'plotOne';
          spot2.plotID = 'er65ds';
          callback(FirebaseTestsHelper.createFakePayload([spot1]));
        }
      };
    }
    );
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglePlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load constructor', () => {
    expect(component.databaseService).toBeTruthy();
    expect(component.authService).toBeTruthy();
    expect(component.router).toBeTruthy();
    expect(component.activatedRoute).toBeTruthy();
    expect(component.modalService).toBeTruthy();
  });

  it('should call addParkingSpot from service', () => {
    spyOn(component.databaseService, 'addParkingSpotToDatabase').and.callFake(() => {
    });
    component.addPkwSpot(1);
    expect(component.databaseService.addParkingSpotToDatabase).toHaveBeenCalled();
  });

  it('should call getSpotList from service', () => {
    fakeSpotListCall();
    component.getSpotsForPlot();
    expect(component.databaseService.getSpotList).toHaveBeenCalled();
  });
});
