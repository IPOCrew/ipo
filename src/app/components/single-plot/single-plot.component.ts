import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FirebaseDatabaseService} from '../../services/firebase-database.service';
import {MParkingSpot} from '../../models/mParkingSpot';
import {v4 as uuid} from 'uuid';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MPlot} from '../../models/mPlot';
import {ActivatedRoute, Router} from '@angular/router';
import {MTenant} from '../../models/mTenant';
import {Observable, Subscription} from 'rxjs';
import {FirebaseAuthService} from '../../services/firebase-auth.service';
import Swal from 'sweetalert2';
import 'rxjs/add/operator/first';

@Component({
  selector: 'app-single-plot',
  templateUrl: './single-plot.component.html',
  styleUrls: ['./single-plot.component.css']
})
export class SinglePlotComponent implements OnInit, OnDestroy {

  actualPlot: MPlot = new MPlot();
  reworkPlot: MPlot = new MPlot();
  newSpot: MParkingSpot = new MParkingSpot();
  carSpots: MParkingSpot[] = [];
  truckSpots: MParkingSpot[] = [];
  tenants: MTenant[] = [];
  plotSubscription: Subscription;
  selectedSpot: MParkingSpot = new MParkingSpot();
  selectedTenant: MTenant = new MTenant();
  selectedPrice = 0;
  user: any;
  usedCarSpots: number;
  usedTruckSpots: number;
  carSpotsToDelete: MParkingSpot[] = [];
  truckSpotsToDelete: MParkingSpot[] = [];
  usedTruckSpotsArray: MParkingSpot[] = [];
  usedCarSpotsArray: MParkingSpot[] = [];
  selectedStart: string;
  selectedEnd: string;
  date: string;
  tenantSubscription: Subscription;
  allTenants: any;

  constructor(public databaseService: FirebaseDatabaseService,
              public authService: FirebaseAuthService,
              public modalService: NgbModal,
              public activatedRoute: ActivatedRoute,
              public router: Router) {
  }

  @ViewChild('contentModal') modal0: any;
  @ViewChild('blockModal') modal1: any;
  @ViewChild('tenantModal') modal2: any;

  ngOnInit() {
    this.authService.getAuthState().first().subscribe(user => {
      if (user) {
        this.user = user;
      } else {
        this.router.navigate(['login']);
      }
    });
    this.activatedRoute.params.subscribe((params) => {
      this.plotSubscription = this.databaseService.getCurrentPlot(params['id']).subscribe((plot) => {
        this.actualPlot = plot.payload.val();
        this.getSpotsForPlot();
      });
    });
    this.getTenantsForUsers();
    const today = new Date();
    this.date = today.getFullYear() + '-' + String(today.getMonth() + 1).padStart(2, '0') + '-' + String(today.getDate()).padStart(2, '0');
    this.loadSubscribe();
  }

  loadSubscribe() {
    this.tenantSubscription = this.databaseService.getTenants().subscribe(res => {
      this.allTenants = res;
    });
  }

  getTenantsForUsers(): void {
    const observable: Observable<any> = this.databaseService.getTenantsForUser();
    observable.first().subscribe(tenants => {
      this.tenants = [];
      tenants.forEach(tenant => {
        if (tenant.userId === this.authService.getCurrentUserID()) {
          this.tenants.push(tenant);
        }
      });
    });
  }

  addPkwSpot(difPkw) {
    while (difPkw > 0) {
      this.newSpot = new MParkingSpot(uuid(),
        this.actualPlot.id,
        'Car',
        false,
        '',
        [new MTenant('', '', '', '', '', '', '', '', '', '', 0)],
        '',
        '',
        '',
        0);
      this.databaseService.addParkingSpotToDatabase(this.newSpot);
      difPkw -= 1;
    }
  }

  getEmptyPkwSpots(difPkw) {
    this.databaseService.getSpotList().first().subscribe((list) => {
      list.forEach((item) => {
        if (item.payload.val().plotID === this.actualPlot.id) {
          if (item.payload.val().currentTenantID === '') {
            if (item.payload.val().tenant.length !== 1) {
              this.usedCarSpotsArray.push(item.payload.val());
            } else if (item.payload.val().vehicleType === 'Car') {
              this.carSpotsToDelete.push(item.payload.val());
            }
          }
        }
      });
      this.removePkwSpot(difPkw);
    });
  }

  getEmptyLkwSpots(difLkw) {
    this.databaseService.getSpotList().first().subscribe((list) => {
      list.forEach((item) => {
        if (item.payload.val().plotID === this.actualPlot.id) {
          if (item.payload.val().currentTenantID === '') {
            if (item.payload.val().tenant.length !== 1) {
              this.usedTruckSpotsArray.push(item.payload.val());
            } else if (item.payload.val().vehicleType === 'Truck') {
              this.truckSpotsToDelete.push(item.payload.val());
            }
          }
        }
      });
      this.removeLkwSpot(difLkw);
    });
  }

  removePkwSpot(difPkw) {
    let showAlert = true;
    if (this.carSpotsToDelete.length === 0) {
      this.popup('Ohhh nooo:(', 'Please unrent some spots before shrinking.');
    } else {
      this.carSpotsToDelete.forEach(spot => {
        if (difPkw > 0 && difPkw <= this.carSpotsToDelete.length) {
          this.databaseService.deleteParkingSpotFromDatabase(spot);
          difPkw--;
        } else if (difPkw !== 0) {
          if (showAlert) {
            this.popup('Ufff...', 'Not enough spots');
            showAlert = false;
          }
        }
      });
    }
    this.carSpotsToDelete = [];
    this.usedCarSpotsArray = [];
  }

  removeLkwSpot(difLkw) {
    let showAlert = true;
    if (this.truckSpotsToDelete.length === 0) {
      this.popup('Ohhh nooo:(', 'Please unrent some spots before shrinking.');
    } else {
      this.truckSpotsToDelete.forEach(spot => {
        if (difLkw > 0 && difLkw <= this.truckSpotsToDelete.length) {
          this.databaseService.deleteParkingSpotFromDatabase(spot);
          difLkw--;
        } else if (difLkw !== 0) {
          console.log(this.truckSpotsToDelete);
          if (showAlert) {
            this.popup('Ufff...', 'Not enough spots');
            showAlert = false;
          }
        }
      });
    }
    this.truckSpotsToDelete = [];
    this.usedTruckSpotsArray = [];
  }

  addLkwSpot(difLkw) {
    while (difLkw > 0) {
      this.newSpot = new MParkingSpot(uuid(),
        this.actualPlot.id,
        'Truck',
        false,
        '',
        [new MTenant('', '', '', '', '', '', '', '', '', '', 0)],
        '',
        '',
        '',
        0);
      this.databaseService.addParkingSpotToDatabase(this.newSpot);
      difLkw -= 1;
    }
  }

  editPlot() {
    if (!this.plotChanged()) {
      this.closeModal();
      return;
    } else {
      if (this.reworkPlot.plotPKW > this.carSpots.length) {
        this.m_addPkwSpots(this.reworkPlot.plotPKW, this.carSpots.length);
      } else if (this.reworkPlot.plotPKW < this.carSpots.length) {
        this.m_removePkwSpots(this.reworkPlot.plotPKW, this.carSpots.length);
      }
      if (this.reworkPlot.plotLKW > this.truckSpots.length) {
        this.m_addLkwSpots(this.reworkPlot.plotLKW, this.truckSpots.length);
      } else if (this.reworkPlot.plotLKW < this.truckSpots.length) {
        this.m_removeLkwSpots(this.reworkPlot.plotLKW, this.truckSpots.length);
      }
      this.reworkPlot.id = this.actualPlot.id;
      this.reworkPlot.address = this.actualPlot.address;
      this.reworkPlot.userID = this.actualPlot.userID;
      if (this.reworkPlot.name === undefined) {
        this.reworkPlot.name = this.actualPlot.name;
      }
      if (this.reworkPlot.plotPKW === undefined) {
        this.reworkPlot.plotPKW = this.actualPlot.plotPKW;
      }
      if (this.reworkPlot.plotLKW === undefined) {
        this.reworkPlot.plotLKW = this.actualPlot.plotLKW;
      }
      this.databaseService.replacePlot(this.actualPlot.id, this.reworkPlot);
      this.reworkPlot = new MPlot();
      this.closeModal();
    }
  }

  plotChanged(): boolean {
    const nameChanged = this.reworkPlot.name !== undefined &&
      this.reworkPlot.name !== ''; // changed
    const pkwChanged = this.reworkPlot.plotPKW !== undefined &&
      this.reworkPlot.plotPKW !== null && this.reworkPlot.plotPKW !== this.carSpots.length;
    const lkwChanged = this.reworkPlot.plotLKW !== undefined &&
      this.reworkPlot.plotLKW !== null && this.reworkPlot.plotLKW !== this.truckSpots.length;
    const changed = nameChanged || pkwChanged || lkwChanged;
    return changed;
  }

  m_addPkwSpots(wantedSpots: number, currentSpots: number) {
    const difPKW = wantedSpots - currentSpots;
    this.addPkwSpot(difPKW);
  }

  m_addLkwSpots(wantedSpots: number, currentSpots: number) {
    const difLKW = wantedSpots - currentSpots;
    this.addLkwSpot(difLKW);
  }

  m_removePkwSpots(wantedSpots: number, currentSpots: number) {
    const difPKW = currentSpots - wantedSpots;
    this.getEmptyPkwSpots(difPKW);
  }

  m_removeLkwSpots(wantedSpots: number, currentSpots: number) {
    const difLKW = currentSpots - wantedSpots;
    this.getEmptyLkwSpots(difLKW);
  }

  getSpotsForPlot() {

    if (this.date === '') {
      const today = new Date();
      this.date = today.getFullYear() + '-' +
        String(today.getMonth() + 1).padStart(2, '0') + '-' +
        String(today.getDate()).padStart(2, '0');
    }
    const self = this;
    this.databaseService.getSpotList().subscribe((list) => {
      this.carSpots = [];
      this.truckSpots = [];
      let spot: MParkingSpot;
      this.usedCarSpots = 0;
      this.usedTruckSpots = 0;
      list.forEach((item) => {
        spot = item.payload.val();
        if (item.payload.val().plotID === this.actualPlot.id) {
          if (item.payload.val().vehicleType === 'Car') {
            if (item.payload.val().tenant.length !== 1) {
              for (const key in item.payload.val().tenant) {
                if (item.payload.val().tenant[key].rentFrom <= self.date && item.payload.val().tenant[key].rentTo >= self.date) {
                  this.usedCarSpots++;
                  console.log(item.payload.val().tenant[key]);
                  spot.currentTenantName = item.payload.val().tenant[key].name;
                  spot.currentTenantPrice = item.payload.val().tenant[key].price;
                  spot.currentTenantID = key;
                  spot.currentTime = item.payload.val().tenant[key].rentFrom + ' - ' + item.payload.val().tenant[key].rentTo;
                }
              }
            }
            this.carSpots.push(spot);
          } else if (item.payload.val().vehicleType === 'Truck') {
            if (item.payload.val().tenant.length !== 1) {
              for (const key in item.payload.val().tenant) {
                if (item.payload.val().tenant[key].rentFrom <= self.date && item.payload.val().tenant[key].rentTo >= self.date) {
                  this.usedTruckSpots++;
                  spot.currentTenantName = item.payload.val().tenant[key].name;
                  spot.currentTenantPrice = item.payload.val().tenant[key].price;
                  spot.currentTenantID = key;
                  spot.currentTime = item.payload.val().tenant[key].rentFrom + ' - ' + item.payload.val().tenant[key].rentTo;
                }
              }
            }
            this.truckSpots.push(spot);
          }
        }
      });
    });
  }

  openModal(content: string, spotId?: string) {
    if (spotId !== undefined) {
      this.databaseService.getCurrentSpot(spotId).first().subscribe((spot) => {
        this.selectedSpot = spot.payload.val();
      });
    }
    this.modalService.open(content);
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  popup(title: string, reason: string) {
    Swal.fire(title, reason, 'info');
  }


  saveTenant(): void {

    if (this.selectedTenant === undefined || this.selectedStart === undefined ||
      this.selectedStart === '' || this.selectedEnd === undefined ||
      this.selectedEnd === '' || this.selectedPrice === undefined || this.selectedPrice === 0) {
      return;
    }

    let free: boolean;
    free = true;
    if (this.selectedSpot.tenant.length !== 1) {
      for (const key in this.selectedSpot.tenant) {
        if (this.selectedStart <= this.selectedSpot.tenant[key].rentFrom &&
          this.selectedEnd <= this.selectedSpot.tenant[key].rentFrom
          || this.selectedStart >= this.selectedSpot.tenant[key].rentTo
          && this.selectedEnd >= this.selectedSpot.tenant[key].rentTo) {
        } else {
          free = false;
        }
      }
    } else {
      free = true;
    }

    if (free === true) {
      this.selectedTenant.rentFrom = this.selectedStart;
      this.selectedTenant.rentTo = this.selectedEnd;
      this.selectedTenant.price = this.selectedPrice;
      const id = uuid();
      this.databaseService.addTenantToParkingSpot(id, this.selectedTenant, this.selectedSpot);

    } else {
      this.popup('Taken', 'This plot is already taken in the given time span.');
    }
    this.closeModal();
  }

  removeTenant(tenantId: string, spotId: string) {
    console.log(tenantId, spotId);
    this.databaseService.getCurrentSpot(spotId).first().subscribe((spot) => {
      this.selectedSpot = spot.payload.val();
      this.selectedSpot.currentTenantPrice = 0;
      this.selectedSpot.currentTenantName = '';
      this.selectedSpot.currentTenantID = '';
      this.databaseService.updateSpot(this.selectedSpot);
      this.databaseService.removeTenantFromParkingSpot(tenantId, spotId);
    });
  }

  ngOnDestroy(): void {
    if (this.plotSubscription) {
      this.plotSubscription.unsubscribe();
    }
  }

  closeSpot() {
    this.selectedSpot.isBlocked = true;
    this.databaseService.addParkingSpotToDatabase(this.selectedSpot);
    this.closeModal();
  }

  openSpot(spot: MParkingSpot) {
    this.selectedSpot = spot;
    this.selectedSpot.isBlocked = false;
    this.selectedSpot.blockReason = '';
    this.databaseService.addParkingSpotToDatabase(this.selectedSpot);
  }
}
