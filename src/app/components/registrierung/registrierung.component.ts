import {Component, OnInit} from '@angular/core';
import {MUser} from '../../models/mUser';
import {FirebaseDatabaseService} from '../../services/firebase-database.service';
import {FirebaseAuthService} from '../../services/firebase-auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import Swal from 'sweetalert2';

class Credentials {
  email: string;
  password: string;
}

@Component({
  selector: 'app-registrierung',
  templateUrl: './registrierung.component.html',
  styleUrls: ['./registrierung.component.css']
})
export class RegistrierungComponent implements OnInit {
  credentials: Credentials = new Credentials();
  user: any;
  firstName = '';
  lastName = '';
  email = '';
  company = '';
  password = '';
  passwordRepeat = '';
  id: number;

  constructor(public firebaseDatabaseService: FirebaseDatabaseService,
              public authService: FirebaseAuthService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
    this.activatedRoute.params.subscribe((params) => {
      this.id = params['id'];
    });
  }

  ngOnInit() {
    this.authService.getAuthState().first().subscribe(user => {
      if (user) {
        this.router.navigate(['main-page']);
      } else {
        console.log('not logged in');
      }
    });
  }

  async register() {
    this.authService.logOut();
    if (!this.checkName()) {
      this.popup('ARGGG...', 'Please enter your names! They must be at least 3 letters long.');
      return;
    }
    if (!this.checkEmail()) {
      this.popup('DANG...', 'Please enter a valid Email');
      return;
    }
    if (!this.checkPasswords()) {
      this.popup('UUHM...', 'I believe your passwords do not match.');
      return;
    }
    this.credentials.email = this.email;
    this.credentials.password = this.password;
    await this.authService.createUser(this.credentials);
    await this.authService.signIn(this.credentials);
    await this.firebaseDatabaseService.addUserToDatabase(new MUser(this.authService.getCurrentUserID(),
      this.firstName, this.lastName, this.email, this.company));
    this.router.navigate(['login']);
    this.popup('PEW PEW!', 'Check your E-mail, we have sent you your verification!');
  }

  private checkName(): boolean {
    return this.firstName.length > 2 && this.lastName.length > 2;
  }

  private checkEmail(): boolean {
    let valid = true;
    if (!this.email.includes('@')) {
      valid = false;
    }
    if (!this.email.includes('.')) {
      valid = false;
    }
    if (this.email.length < 5) {
      valid = false;
    }
    return valid;
  }

  private checkPasswords(): boolean {
    return this.password === this.passwordRepeat;
  }

  popup(title: string, reason: string) {
    Swal.fire(title, reason, 'info');
  }
}
