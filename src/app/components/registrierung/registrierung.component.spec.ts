import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrierungComponent } from './registrierung.component';
import { AppModule } from '../../app.module';
import { By } from '@angular/platform-browser';
import { FirebaseDatabaseService } from 'src/app/services/firebase-database.service';
import { FirebaseAuthService } from 'src/app/services/firebase-auth.service';
import { RouteConfigLoadStart, ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

describe('RegistrierungComponent', () => {
  let component: RegistrierungComponent;
  let fixture: ComponentFixture<RegistrierungComponent>;
  let authService: FirebaseAuthService;
  let route: ActivatedRoute;
  let router: Router;
  let databaseService: FirebaseDatabaseService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterTestingModule],
      providers: [FirebaseDatabaseService, FirebaseAuthService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    authService = TestBed.get(FirebaseAuthService);
    route = TestBed.get(ActivatedRoute);
    router = TestBed.get(Router);
    databaseService = TestBed.get(FirebaseDatabaseService);
    spyOn(authService, 'getAuthState').and.returnValue(of([]));
    spyOn(route, 'params').and.returnValue(of(['']));
    spyOn(router, 'navigate').and.callFake(() => { });

    fixture = TestBed.createComponent(RegistrierungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    fixture.ngZone.run(() => {
      fixture.detectChanges();
      expect(component).toBeTruthy();
    });
  });

  it('should not register when name is to short register', () => {
    const authSpy = spyOn(authService, 'logOut').and.callFake(() => { });
    component.firstName = '';
    component.lastName = 'lastname';
    component.register();
    expect(authSpy).toHaveBeenCalled();
  });

  it('should not register when mail is invalid', () => {
    const authSpy = spyOn(authService, 'logOut').and.callFake(() => { });
    component.firstName = 'firstname';
    component.lastName = 'lastname';
    component.email = 'no at in this mail';
    component.register();
    expect(authSpy).toHaveBeenCalled();
  });

  it('should send verification when input is correct', () => {
    const authCreateSpy = spyOn(authService, 'createUser').and.callFake(() => { });
    const signInSpy = spyOn(authService, 'signIn').and.callFake(() => { });
    const dbSpy = spyOn(databaseService, 'addUserToDatabase').and.callFake(() => { });
    component.firstName = 'firstname';
    component.lastName = 'lastname';
    component.password = 'password';
    component.passwordRepeat = 'password';
    component.email = 'test@test.de';
    component.register();
    //expect(dbSpy).toHaveBeenCalled();
  });
  /*
  it('should get firstname', () => {
    let input = fixture.debugElement.query(By.css('#name')).nativeElement.value;
    expect(input).toBe('');
    input = 'Peter';
    expect(input).toBe('Peter');
  });

  it('should start with empty Credentials', () => {
    expect(component.credentials.email).toBeFalsy();
    expect(component.credentials.password).toBeFalsy();
  });
  */

  /*
  it('should call onSubmit', () => {
    spyOn(component, 'onSubmit');
    component.onSubmit();
    expect(component.onSubmit).toHaveBeenCalled();
  });
  */

  /*
  it('should call authService', () => {

  });

  it('should call databaseService', () => {
    spyOn(component, 'authService');
    spyOn(component, 'firebaseDatabaseService');
    component.email = 'test@test.de';
    component.password = '12345678';
    component.passwordRepeat = '12345678';
    component.onSubmit();
    expect(component.firebaseDatabaseService.addUserToDatabase).toBeDefined();
  });

  it('should fire alert', () => {

    component.onSubmit();

  });
  */
});
