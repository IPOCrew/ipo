import {Component, Injectable, Optional} from '@angular/core';
import Swal from 'sweetalert2';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-cookies',
  templateUrl: './cookies.component.html',
  styleUrls: ['./cookies.component.css']
})

@Injectable({
  providedIn: 'root'
})

export class CookiesComponent {
  constructor(@Optional() public cookieService: CookieService) {
    console.log('constactor');
  }

  get cookieExists(): boolean {
    return this.cookieService.check('accepted');
  }

  get isPrivacyAccepted(): boolean {
    return this.cookieService.check('privacy-accepted');
  }

  showCookiesRequest() {
    if (this.cookieExists === false) {
      Swal.fire({
        confirmButtonColor: '#d4ac0d',
        allowOutsideClick: false,
        width: 600,
        customClass: 'sweet-alert',
        position: 'bottom',
        title: 'Cookies',
        text: 'We use cookies!',
        cancelButtonText: 'About cookies',
        showCancelButton: true,
        confirmButtonText: 'Yes ,I accept it',
      }).then((result) => {
        if (result.value) {
          this.cookieService.set('accepted', 'accepted');
          this.thanksAlert();
        } else if (result.dismiss) {
          this.cookiesDescription();
        }
      });
    }
  }

  thanksAlert() {
    Swal.fire({
      allowOutsideClick: false,
      title: 'Thanks!',
      type: 'success',
      timer: 1400,
      showConfirmButton: false
    });
  }

  cookiesDescription() {
    Swal.fire({
      allowOutsideClick: false,
      width: 800,
      confirmButtonColor: '#d4ac0d',
      customClass: 'sweet-alert',
      position: 'center',
      title: 'What are cookies',
      text: 'Cookies are small files which are stored on a user´s computer.' +
        '  They are designed to hold a modest amount of data specific to a particular client and website,\n' +
        ' and can be accessed either by the web server or the client computer.\n' +
        '   This allows the server to deliver a page tailored to a particular user,\n' +
        ' or the page itself can contain some script which is aware of the data in the cookie\n' +
        'and so is able to carry information from one visit to the website (or related site) to the next.\n',
      type: 'info',
      cancelButtonText: 'About cookies',
      confirmButtonText: 'Yes ,I accept it',
      showCloseButton: true,
    }).then((result) => {
      if (result.value) {
        this.cookieService.set('accepted', 'accepted');
        this.thanksAlert();
      }
    });
  }


}
