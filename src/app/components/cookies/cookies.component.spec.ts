import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CookiesComponent } from './cookies.component';
import { CookieService } from 'ngx-cookie-service';
import Swal from 'sweetalert2';
import { of } from 'rxjs';

describe('CookiesComponent', () => {
  let component: CookiesComponent;
  let fixture: ComponentFixture<CookiesComponent>;
  let cookieService: CookieService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CookiesComponent],
      providers: [CookieService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CookiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    cookieService = fixture.debugElement.injector.get(CookieService);
    cookieService.deleteAll();
    component.cookieService = cookieService;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return boolean on PrivacyAccept', () => {
    expect(component.isPrivacyAccepted).toBe(false);
  });

  it('Should show a thanks alert', () => {
    const spy = spyOn(component, 'thanksAlert').and.callThrough();
    component.thanksAlert();
    expect(spy).toHaveBeenCalled();
  });

  it('should acceppt on cookie description', () => {
    const spy = spyOn(component, 'cookiesDescription').and.callThrough();
    const swalSpy = spyOn(Swal, 'fire').and.returnValue(Promise.resolve({ value: true }));
    component.cookiesDescription();

    expect(spy).toHaveBeenCalled();
  });

  it('should acceppt on cookie dialog', () => {
    const spy = spyOn(component, 'showCookiesRequest').and.callThrough();
    const swalSpy = spyOn(Swal, 'fire').and.returnValue(Promise.resolve({ value: true }));
    component.showCookiesRequest();

    expect(spy).toHaveBeenCalled();
  });

  it('should show cookie Description on decline', () => {
    const spy = spyOn(component, 'showCookiesRequest').and.callThrough();
    const descSpy = spyOn(component, 'cookiesDescription').and.returnValue(null);
    const swalSpy = spyOn(Swal, 'fire').and.returnValue(Promise.resolve({ dismiss: true, value: false }));
    component.showCookiesRequest();

    expect(spy).toHaveBeenCalled();
    //expect(descSpy).toHaveBeenCalled();
  });

  it('should show cookie request', () => {
    component.showCookiesRequest();
  });

});
