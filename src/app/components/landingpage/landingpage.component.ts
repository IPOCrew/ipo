import {Component, OnInit} from '@angular/core';
import {FirebaseAuthService} from '../../services/firebase-auth.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {CookiesComponent} from '../cookies/cookies.component';

@Component({
  selector: 'app-landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.css']
})
export class LandingpageComponent implements OnInit {
  user: any;

  constructor(public authService: FirebaseAuthService, public router: Router, private cookies: CookiesComponent) {
  }

  ngOnInit() {
    this.authService.getAuthState().first().subscribe(user => {
      if (user) {
        this.router.navigate(['main-page']);
      } else {
        console.log('not logged in');
        this.cookies.showCookiesRequest();
      }
    });
  }

  popup() {
    Swal.fire('Maintenance!', 'Paid versions are currently not available :(', 'info');
  }
}
