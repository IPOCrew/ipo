export class MUser {

  constructor(public id?: string, public firstName?: string, public lastName?: string,
              public email?: string, public company?: string) {

  }

}
