import {MTenant} from './mTenant';

export class MParkingSpot {

  constructor(public id?: string,
              public plotID?: string,
              public vehicleType?: string,
              public isBlocked?: boolean,
              public blockReason?: string,
              public tenant?: MTenant[],
              public currentTenantName?: string,
              public currentTenantID?: string,
              public currentTime?: string,
              public currentTenantPrice?: number) {
  }
}
