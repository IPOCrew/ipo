export class MTenant {
  constructor(public id?: string,
              public name?: string,
              public lastname?: string,
              public userId?: string,
              public adress?: string,
              public email?: string,
              public licensePlate?: string,
              public phone?: string,
              public rentFrom?: string,
              public rentTo?: string,
              public price?: number) {}
}
