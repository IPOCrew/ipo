import {NgModule} from '@angular/core';
import {environment} from '../environments/environment';
import {APP_BASE_HREF} from '@angular/common';
import {GrdFilterPipe} from './components/filter/GrdFilterPipe';
// Module Imports
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LayoutModule} from '@angular/cdk/layout';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {MatButtonModule, MatDialogModule, MatIconModule} from '@angular/material';
import {AngularFireModule} from '@angular/fire';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {BrowserModule} from '@angular/platform-browser';
// Component Imports
import {AlertComponent} from './components/footer/alert/alert.component';
import {SinglePlotComponent} from './components/single-plot/single-plot.component';
import {LandingpageComponent} from './components/landingpage/landingpage.component';
import {FooterDialogComponent} from './components/footer/footer.dialog.component';
import {PrivacyComponent} from './components/footer/dialogs/privacy/privacy.component';
import {ContactComponent} from './components/footer/dialogs/contact/contact.component';
import {AboutComponent} from './components/footer/dialogs/about/about.component';
import {ImprintComponent} from './components/footer/dialogs/imprint/imprint.component';
import {MainPageComponent} from './components/main-page/main-page.component';
import {RegistrierungComponent} from './components/registrierung/registrierung.component';
import {AppComponent} from './app.component';
import {LoginComponent} from './components/login/login.component';
import {CustomerManagementComponent} from './components/customer-management/customer-management.component';
import {CookiesComponent} from './components/cookies/cookies.component';
// Validator Imports
import {RegValidationDirective} from './validator/reg-validation.directive';
import {MinValidatorDirective} from './validator/min-validator.directive';
// Service Imports
import {FirebaseDatabaseService} from './services/firebase-database.service';
import {FirebaseAuthService} from './services/firebase-auth.service';
import {CookieService} from 'ngx-cookie-service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LandingpageComponent,
    RegistrierungComponent,
    FooterDialogComponent,
    PrivacyComponent,
    ContactComponent,
    AboutComponent,
    ImprintComponent,
    AlertComponent,
    SinglePlotComponent,
    MainPageComponent,
    RegValidationDirective,
    MinValidatorDirective,
    GrdFilterPipe,
    CookiesComponent,
    CustomerManagementComponent
  ],
  entryComponents: [FooterDialogComponent, PrivacyComponent,
    ContactComponent, ImprintComponent, AboutComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    ReactiveFormsModule,

  ],
  providers: [
    MatDialogModule,
    CookieService,
    FirebaseDatabaseService,
    FirebaseAuthService,
    {provide: APP_BASE_HREF, useValue: '/'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
