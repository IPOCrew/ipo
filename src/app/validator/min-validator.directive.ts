import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from '@angular/forms';

@Directive({
  selector: '[appInputMin]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: MinValidatorDirective, multi: true}
  ]
})
export class MinValidatorDirective implements Validator {
  @Input() appInputMin: number;

  constructor() {
  }

  registerOnValidatorChange(fn: () => void): void {
  }

  validate(control: AbstractControl): ValidationErrors | null {
    const currentValue = control.value;
    if (currentValue !== null) {
      return currentValue.length > this.appInputMin ? null : {
        min: {
          Valid: false
        }
      };
    } else {
      return null;
    }
  }
}
