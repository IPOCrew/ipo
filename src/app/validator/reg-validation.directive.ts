import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from '@angular/forms';

@Directive({
  selector: '[appInputReg]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: RegValidationDirective, multi: true}
  ]

})
export class RegValidationDirective implements Validator {
  @Input() appInputReg: string;

  constructor() {
  }

  registerOnValidatorChange(fn: () => void): void {
  }

  validate(control: AbstractControl): ValidationErrors | null {
    const currentValue = control.value;
    let isValid;
    const textReg = /^[a-zA-ZäöüÄÖÜ ]*$/;
    const emailReg = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (this.appInputReg === 'text') {
      isValid = textReg.test(currentValue);
    } else if (this.appInputReg === 'email') {
      isValid = emailReg.test(currentValue);
    }
    return isValid ? null : {
      reg: {
        Valid: false
      }
    };
  }

}
