// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAIVfjk2ddDdsGncHGTyId97e7fSnzcBzA',
    authDomain: 'ipocrew-a004d.firebaseapp.com',
    databaseURL: 'https://ipocrew-a004d.firebaseio.com',
    projectId: 'ipocrew-a004d',
    storageBucket: 'ipocrew-a004d.appspot.com',
    messagingSenderId: '29287732207'

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
